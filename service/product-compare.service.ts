import { Injectable } from '@angular/core';
import {Constants} from "../class/constants";

const rules = {};

rules[Constants.FamilyRJ45Id] = [
    {id: 2, type: "range", name: "AWG"},
    {id: 6, compareWithId: 17, type: "less", name: "OuterDiameter, mm", compareWith: "MaxCable OD, mm"},
    {id: 5, type: "equal", name: "Solid/Stranded"},
    {id: 1, type: "equal", name: "Category"}
];

rules[Constants.FamilyRFId] = [
    {id: 27, type: "collection", conditions: [{type: "absent", field: "userId"}], name: "Cable Group"},
    {id: 66, compareWithId: 39, type: "moreMM/Inch", conditions: [{type: "present", field: "userId"}], name: "Central Pin OD", compareWith: "Center Conductor"},
    {id: 67, compareWithId: 40, type: "moreMM/Inch", conditions: [{type: "present", field: "userId"}], name: "Ferule OD", compareWith: "Jacket"}
];

rules[Constants.FamilyIDCId] = [
    {id: 5, type: "equal", name: "Solid/Stranded"},
    {id: 53, type: "equal", name: "Strain Relief"},
    {id: 63, compareWithId: 51, type: "equal", name: "Pitch", compareWith: "Flat Cable Pitch"}
];

rules[Constants.FamilyHeadersId] = [
    {id: 26, type: "equal", name: "Male / Female"}
    // {id: 46, type: "collection", name: "Series Terminal"}
];

rules[Constants.FamilyHeadersId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "range", name: "AWG"}
];

rules[Constants.FamilyCircularId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "range", name: "AWG"},
    {id: 70, compareWithId: 81, type: "lessRange", name: "Nominal Diameter", compareWith: "Cable OD"}
];

rules[Constants.FamilyDTypeSolderId] =
    rules[Constants.FamilyDTypeCrimpId] = [
        {id: 14, type: "equal", name: "Number of Positions"}
    ];

rules[Constants.FamilyDTypeSolderId + '_' + Constants.FamilyAlphaId] =
    rules[Constants.FamilyDTypeCrimpId + '_' + Constants.FamilyAlphaId] = [
        {id: 2, type: "lessMinOrEqualRange", strict: true, conditions: [{type: "category", value: Constants.CategoryConnectorsId}], name: "AWG"},
        {id: 2, type: "moreMinOrEqualRange", strict: true, conditions: [{type: "category", value: [Constants.CategoryCablesId, Constants.CategoryWireId, Constants.CategoryMulticonductorId, Constants.CategoryMiltipairId, Constants.CategoryMulticonductorFlexId, Constants.CategoryMiltipairFlexId]}], name: "AWG"},
        {id: 70, compareWithId: 81, type: "less", name: "Nominal Diameter", compareWith: "Cable OD"},
        {id: 14, type: "equal", name: "Number of Positions"}
    ];

rules[Constants.FamilyAlphaId] = [
    {id: 70, compareWithId: 81, type: "less", name: "Nominal Diameter", compareWith: "Cable OD"},
    {id: 2, type: "lessMinOrEqualRange", strict: true, conditions: [{type: "category", value: Constants.CategoryCrimpTerminal}], name: "AWG"},
    {id: 2, type: "moreMinOrEqualRange", strict: true, conditions: [{type: "category", value: [Constants.CategoryCablesId, Constants.CategoryWireId, Constants.CategoryMulticonductorId, Constants.CategoryMiltipairId, Constants.CategoryMulticonductorFlexId, Constants.CategoryMiltipairFlexId]}], name: "AWG"}
];

rules[Constants.FamilyTerminalBlockId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, compareWithId: 99, type: "range", conditions: [{type: "fieldValue", id: 5, value: "Solid", field: "Solid/Stranded"}], name: "AWG", compareWith: "AWG Solid"},
    {id: 2, compareWithId: 100, type: "range", conditions: [{type: "fieldValue", id: 5, value: "Stranded", field: "Solid/Stranded"}], name: "AWG", compareWith: "AWG Stranded"},
    {id: 69, compareWithId: 95, type: "compareTable", compareTable: ["300__300", "300__600", "600__600", "600__750", "600__800", "1000__1000"], name: "Volt", compareWith: "Rated Voltage UL"}
];

rules[Constants.FamilyTerminalId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "range", name: "AWG"}
];

rules[Constants.FamilyPowerId] = [
    {id: 166, type: "equal", name: "Cable Type", conditions: [{type: "absent", field: "userId"}]},
    {id: 2, type: "equal", name: "AWG", conditions: [{type: "present", field: "userId"}] }
];

rules[Constants.FamilyDinId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "range", name: "AWG"},
    {id: 70, compareWithId: 81, type: "lessOrEqual", name: "Nominal Diameter", compareWith: "Cable OD", conditions: [{type: "fieldValue", id: 28, value: "No", field: "Panel Mounting"}]}
];

rules[Constants.FamilyXlrId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "moreOrEqual", name: "AWG"},
    {id: 70, compareWithId: 81, type: "range", name: "Nominal Diameter", compareWith: "Cable OD"}
];

rules[Constants.FamilyModularDcPowerId + '_' + Constants.FamilyAlphaId] = [
    {id: 70, compareWithId: 81, type: "less", name: "Nominal Diameter", compareWith: "Cable OD"}
];

rules[Constants.FamilyHeadersTerminalId] = [
    {id: 26, type: "equal", name: "Male / Female"}
    // {id: 46, type: "collection", name: "Series Terminal"}
];

rules[Constants.FamilyHeadersTerminalId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "range", name: "AWG"}
];

rules[Constants.FamilyDTypeTerminalId + '_' + Constants.FamilyAlphaId] = [
    {id: 2, type: "range", name: "AWG"}
];


@Injectable({
    providedIn: 'root',
})
export class ProductCompareService {
    private checkConditions(product, conditions) {
        let isSuitConditions = false;

        if(conditions && conditions.length) {
            for(let type in conditions) {
                let condition = conditions[type];
                switch (condition.type) {
                    case 'absent' :
                        isSuitConditions = ! (product[condition.field]);
                        break;
                    case 'present' :
                        isSuitConditions = product[condition.field];
                        break;
                    case 'fieldValue' :
                        let searchField = function (element) {
                            return element.fieldId == condition.id;
                        };

                        let field = product.fields.find(searchField);

                        if(! field) {
                            isSuitConditions = false;
                            break;
                        }

                        isSuitConditions = (field.fieldValue == condition.value);
                        break;
                    case 'category' :
                        isSuitConditions = true;
                        if(product.category) {
                            if(typeof(condition.value) == "object") {
                                isSuitConditions = (condition.value.indexOf(parseInt(product.category.categoryId)) !== -1);
                            } else {
                                isSuitConditions = (product.category.categoryId == condition.value);
                            }
                        }
                        break;
                    case 'type' :
                        isSuitConditions = true;
                        if(product.type) {
                            isSuitConditions = (product.type.typeId == condition.value)
                        }
                        break;
                    default:
                        isSuitConditions = true;
                        break;
                }

                if(! isSuitConditions) {
                    break;
                }
            }
        } else {
            isSuitConditions = true;
        }

        return isSuitConditions;
    }

    compare(product, compareWith) {

        let errorsArray = [];
        let suitable = true;

        if(! compareWith) {
            return errorsArray;
        }

        let filterMap = this.getMapFilters(product.family.familyId, compareWith.family.familyId);

        if(! filterMap || ! compareWith.fields || ! product.fields) {
            return errorsArray;
        }

        let suitableFilters = [];

        for(let filterId in filterMap) {
            let filter = filterMap[filterId];

            let searchCompareField = function (element) {
                if (filter.compareWithId) {
                    return element.fieldId == filter.compareWithId;
                } else {
                    return element.fieldId == filter.id;
                }
            };

            let searchProductField = function (element) {
                return element.fieldId == filter.id;
            };

            let compareFilterField = compareWith.fields.find(searchCompareField);
            let productFilterField = product.fields.find(searchProductField);

            if(! compareFilterField || ! productFilterField) {
                compareFilterField = product.fields.find(searchCompareField);
                productFilterField = compareWith.fields.find(searchProductField);
            }

            if(compareFilterField && productFilterField) {
                if(filter.strict){
                    if(this.checkConditions(product, filter['conditions'])) {
                        suitableFilters.push({
                            filter: filter,
                            productValue: productFilterField,
                            itemValue: compareFilterField
                        });
                    }
                } else {
                    if(this.checkConditions(product, filter['conditions']) || this.checkConditions(compareWith, filter['conditions'])) {
                        suitableFilters.push({
                            filter: filter,
                            productValue: productFilterField,
                            itemValue: compareFilterField
                        });
                    }
                }
            }
        }

        for(let id in suitableFilters) {
            let suitableFilter = suitableFilters[id];

            if(typeof (suitableFilter.filter.type) == "object") {
                for(let key in suitableFilter.filter.type) {
                   suitable = suitable && this.checkValues(suitableFilter.productValue.fieldValue, suitableFilter.itemValue.fieldValue, suitableFilter.filter.type);
                }
            } else {
                //if one of items doesn`t have value
                if(suitableFilter.productValue.fieldValue && suitableFilter.itemValue.fieldValue) {
                    let productValue, itemValue;

                    if(typeof (suitableFilter.productValue.fieldValue) == "object") {
                        productValue = suitableFilter.productValue.fieldValue.value;
                    } else {
                        productValue = suitableFilter.productValue.fieldValue;
                    }

                    if(typeof (suitableFilter.itemValue.fieldValue) == "object") {
                        itemValue = suitableFilter.itemValue.fieldValue.value;
                    } else {
                        itemValue = suitableFilter.itemValue.fieldValue;
                    }

                    suitable = this.checkValues(productValue, itemValue, suitableFilter.filter);
                } else {
                    suitable = false;
                }
            }

            if(! suitable) {
                errorsArray.push(suitableFilter.filter.compareWith ? suitableFilter.filter.compareWith : suitableFilter.filter.name);
            }
        }

        return errorsArray;
    }

    private checkValues(productValue, itemValue, filterParams) {
        let suitable = false;

        switch (filterParams.type) {
            case 'lessOrEqual':
                suitable = (parseFloat(itemValue.replace(/,/, '.')) >= parseFloat(productValue.replace(/,/, '.')));
                break;
            case 'less':
                suitable = (parseFloat(itemValue.replace(/,/, '.')) > parseFloat(productValue.replace(/,/, '.')));
                break;
            case 'moreOrEqual':
                suitable = (parseFloat(itemValue.replace(/,/, '.')) <= parseFloat(productValue.replace(/,/, '.')));
                break;
            case 'more':
                suitable = (parseFloat(itemValue.replace(/,/, '.')) < parseFloat(productValue.replace(/,/, '.')));
                break;
            case 'like':
                suitable = (itemValue.search(productValue) != -1 || productValue.search(itemValue) != -1);
                break;
            case 'range':
                let itemFrom, itemTo, productFrom, productTo;
                if(itemValue.search('-') != -1) {
                    itemFrom = parseFloat(itemValue.split('-')[0]);
                    itemTo = parseFloat(itemValue.split('-')[1]);
                } else {
                    itemFrom = parseFloat(itemValue);
                    itemTo = parseFloat(itemValue);
                }

                if(productValue.search('-') != -1) {
                    productFrom = parseFloat(productValue.split('-')[0]);
                    productTo = parseFloat(productValue.split('-')[1]);
                } else {
                    productFrom = parseFloat(productValue);
                    productTo = parseFloat(productValue);
                }

                suitable = (productFrom <= itemTo && productFrom >= itemFrom)
                    || (productTo <= itemTo && productTo >= itemFrom)
                    || (itemTo <= productTo && itemTo >= productFrom)
                    || (itemFrom <= productTo && itemFrom >= productFrom);
                break;
            case 'collection': {
                let itemValues = itemValue.split(',');
                let productValues = productValue.split(',');

                suitable = false;

                for(let productsKey in productValues) {
                    productValues[productsKey] = productValues[productsKey].trim();
                }

                for(let itemsKey in itemValues) {
                    let value = itemValues[itemsKey].trim();

                    if(value.length) {
                        if(productValues.indexOf(value) != -1) {
                            suitable = true;
                            break;
                        }
                    }
                }

                break;
            }
            case 'lessMM/Inch': {
                let productValueMM = productValue.split('/')[0].trim();
                let itemValueMM = itemValue.split('/')[0].trim();
                suitable = (parseFloat(itemValueMM.replace(/,/, '.')) > parseFloat(productValueMM.replace(/,/, '.')));
                break;
            }
            case 'moreMM/Inch': {
                let productValueMM = productValue.split('/')[0].trim();
                let itemValueMM = itemValue.split('/')[0].trim();
                suitable = (parseFloat(itemValueMM.replace(/,/, '.')) < parseFloat(productValueMM.replace(/,/, '.')));
                break;
            }
            case 'ratio': {
                suitable = parseFloat(productValue.replace(/,/, '.')) == parseFloat(itemValue.replace(/,/, '.')) * filterParams.ratio;
                break;
            }
            case 'compareTable': {
                let table = filterParams.compareTable;
                suitable = (table.indexOf(itemValue + '__' + productValue) != -1 || table.indexOf(productValue + '__' + itemValue) != -1);
                break;
            }
            case 'lessRange': {
                let clonedFilterParams = this.clone(filterParams);

                if(itemValue.search('-') !== -1 || productValue.search('-') !== -1) {
                    clonedFilterParams.type = 'range';
                } else {
                    clonedFilterParams.type = 'less';
                }

                suitable = this.checkValues(productValue, itemValue, clonedFilterParams);
                break;
            }
            case 'moreRange': {
                let clonedFilterParams = this.clone(filterParams);

                if(itemValue.search('-') !== -1 || productValue.search('-') !== -1) {
                    clonedFilterParams.type = 'range';
                } else {
                    clonedFilterParams.type = 'more';
                }

                suitable = this.checkValues(productValue, itemValue, clonedFilterParams);
                break;
            }
            case 'half': {
                suitable = ((parseFloat(itemValue.replace(/,/, '.')) / 2) == parseFloat(productValue.replace(/,/, '.')));
                break;
            }
            case 'lessMinOrEqualRange': {
                let itemMinValue, productMinValue;

                if(itemValue.search('-') != -1) {
                    itemMinValue = parseFloat(itemValue.split('-')[0]);
                } else {
                    itemMinValue = parseFloat(itemValue);
                }

                if(productValue.search('-') != -1) {
                    productMinValue = parseFloat(productValue.split('-')[0]);
                } else {
                    productMinValue = parseFloat(productValue);
                }

                suitable = itemMinValue >= productMinValue;
                break;
            }
            case 'moreMinOrEqualRange': {
                let itemMinValue, productMinValue;

                if(itemValue.search('-') != -1) {
                    itemMinValue = parseFloat(itemValue.split('-')[0]);
                } else {
                    itemMinValue = parseFloat(itemValue);
                }

                if(productValue.search('-') != -1) {
                    productMinValue = parseFloat(productValue.split('-')[0]);
                } else {
                    productMinValue = parseFloat(productValue);
                }

                suitable = itemMinValue <= productMinValue;
                break;
            }
            case 'equal':
            default : {
                suitable = (productValue == itemValue);
            }
        }

        return suitable;
    }

    private getMapFilters (productFamilyId, compareWithFamilyId) {
        if(rules[productFamilyId + "_" + compareWithFamilyId]) {
            return rules[productFamilyId + "_" + compareWithFamilyId];
        } else if(rules[compareWithFamilyId + "_" + productFamilyId]) {
            return rules[compareWithFamilyId + "_" + productFamilyId];
        } else if(rules[productFamilyId]) {
            return rules[productFamilyId]
        } else if(rules[compareWithFamilyId]) {
            return rules[compareWithFamilyId]
        }

        return [];
    }

    private clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;

        let copy = obj.constructor();

        for (let attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }
}