import {Component, ElementRef, OnInit} from '@angular/core';
import template from './family-category-attributes.component.html';
import {HttpClient} from "@angular/common/http";
import {ObjectInstanceOf} from "../../class/objectInstanceOf";
import {Constants} from "../../class/constants";
import {MatTabChangeEvent} from "@angular/material";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

declare interface CategoryAttributes {
    category: Category;
    attributes: Array<any>;
}

declare var $:any;

@Component({
    selector: 'family-category-attributes',
    template: template
})
export class FamilyCategoryAttributesComponent implements OnInit {
    private categoryAttributes: Array<CategoryAttributes> = [];
    private categories = [];
    private attributes = {};
    private familyId:number = parseInt(window.location.pathname.replace( /^\D+/g, ''));
    private multicable = false;

    constructor(private http: HttpClient, private el: ElementRef) {
    }

    ngOnInit(): void {
        this.categoryAttributes = JSON.parse(this.el.nativeElement.attributes.initial.value);
        this.multicable = this.el.nativeElement.attributes.multicable.value;
        let that = this;

        let route: string = "_ajax-category:load-by-family";

        if(this.multicable) {
            route = "_ajax-category:load-only-cable-typed-by-family";
        }

        this.http
            .get(Routing.generate(route, {id: this.familyId}))
            .subscribe(function(results) {
                Object.assign(that.categories, results);

                if(! that.categoryAttributes.length) {
                    for (let category in that.categories) {
                        that.categoryAttributes.push({category: that.categories[category], attributes: []});
                    }

                    if(that.categories[0]) {
                        that.selectedCategory(that.categories[0]['categoryId']);
                    }
                } else {
                    for (let category in that.categories) {
                        let exist = false;

                        for(let index in that.categoryAttributes) {
                            if(that.categoryAttributes[index]['category']['categoryId'] == that.categories[category]['categoryId']) {
                                exist = true;
                            }
                        }

                        if(! exist) {
                            that.categoryAttributes.push({category: that.categories[category], attributes: []});
                        }
                    }
                }

                for (let index = 0; that.categoryAttributes.length > index; index++) {
                    let exist: boolean = false;

                    for (let category in that.categories) {
                        if(that.categoryAttributes[index]['category']['categoryId'] == that.categories[category]['categoryId']) {
                            exist = true;
                        }
                    }

                    if(! exist) {
                        that.categoryAttributes.splice(index, 1);
                    }
                }

                that.loadAttributes(that.categoryAttributes[0].category.categoryId);
            });
    }

    loadAttributes(categoryId) {
        if(! this.attributes[categoryId]) {
            let that = this;
            this.http
                .get(Routing.generate("_ajax-category:load-attributes", {id: categoryId}))
                .subscribe(function (results) {
                    that.attributes[categoryId] = [];
                    Object.assign(that.attributes[categoryId], results);
                });
        }
    };

    selectedCategory(event: MatTabChangeEvent) {
        if(this.categoryAttributes[event.index]) {
            this.loadAttributes(this.categoryAttributes[event.index].category.categoryId);

            if(this.categoryAttributes[event.index].category.categoryId == Constants.CategoryConnectorsId) {
                $("#family_molded").closest("div.checkbox").fadeIn();
            } else {
                $("#family_molded").closest("div.checkbox").fadeOut();
            }
        }
    };


    trans(message): string {
        return Translator.trans(message);
    }

    compareObjects(o1: any, o2: any): boolean {
        return ObjectInstanceOf.compareObjects(o1, o2);
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
        }
    }
}
