declare var Routing: any;
declare var Translator: any;

declare interface Family {
    familyId: number;
    familyName: string;
}

declare interface Category {
    categoryId: number;
    categoryName: string;
}