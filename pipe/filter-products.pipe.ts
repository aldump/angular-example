import { Pipe, PipeTransform } from '@angular/core';
import {CheckConnectedWithService} from "../service/check-connected-with.service";

@Pipe({name: 'filter-products'})
export class FilterProductsPipe implements PipeTransform {
    constructor(private checkConnectedWith: CheckConnectedWithService) {

    }

    transform(collection: Array<any>, selectedItems, selectedItem): Array<any> {
        let that = this;
        if(! collection) {
            return [];
        }

        return collection.filter(function(item) {
            let errors = that.checkConnectedWith.check(selectedItem, selectedItems, item);

            return errors.length === 0;
        });
    }
}