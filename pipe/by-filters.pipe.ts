import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'byFilters', pure: false})
export class ByFiltersPipe implements PipeTransform {
    transform(collection: Array<any>, selectedFilters): Array<any> {
        if(! collection) {
            return [];
        }

        return collection.filter(function(item) {
            if(selectedFilters) {
                for(let key in selectedFilters) {
                    if(selectedFilters[key] == null || selectedFilters[key].length == 0) {
                        continue;
                    }

                    let field = null;

                    for(let fieldId in item.fields) {
                        field = item.fields[fieldId];

                        if(field.fieldId != key) {
                            field = null;
                        } else {
                            break;
                        }
                    }

                    if (field && field.fieldValue != selectedFilters[key]) {
                        return false;
                    }
                }
            }

            return true;
        });
    }
}