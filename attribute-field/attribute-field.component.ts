import {Component, ElementRef, OnInit} from '@angular/core';
import template from './attribute-field.component.html';
import {HttpClient} from "@angular/common/http";

declare interface FieldOptions {
    options: object;
    type?: string|null;
}

@Component({
    selector: 'attribute-field',
    template: template
})
export class AttributeFieldComponent implements OnInit {
    private fieldOptions: FieldOptions = {options:{}};
    private fieldTypes = {};
    private fieldAdditionalOptions = {};
    private objectKeys = Object.keys;

    constructor(private http: HttpClient, private el: ElementRef) { }

    loadAdditionalOptions(): void {
        this.fieldOptions.options = {};
        if(! this.fieldAdditionalOptions[this.fieldOptions.type]) {
            this.loadOptions();
        }
    };

    loadOptions(): void {
        let that = this;
        this.fieldAdditionalOptions[this.fieldOptions.type] = {};
        this.http
            .get(Routing.generate("_ajax-attribute:load-field-options", {type: this.fieldOptions.type}))
            .subscribe(function (results) {
                Object.assign(that.fieldAdditionalOptions[that.fieldOptions.type], results);
            })
        ;
    };

    trans(message): string {
        return Translator.trans(message);
    }

    ngOnInit(): void {
        this.fieldOptions = JSON.parse(this.el.nativeElement.attributes.initial.value);

        if(this.fieldOptions.type) {
            this.loadOptions();
        }

        let that = this;
        this.http.get(Routing.generate("_ajax-attribute:load-field-types"))
            .subscribe(function(results) {
                Object.assign(that.fieldTypes, results);
            })
        ;
    }
}
